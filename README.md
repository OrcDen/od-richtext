# <od-richtext>

> A wrapper around quill to be used in shadow DOM projects and components

`<od-richtext>` is similar to an input textarea however it comes with a variety of styling options to format he users input. The component outputs semantic HTML and styling to go with it.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-richtext
```
## Usage
```
import '@orcden/od-richtext';
```
```
<od-richtext></od-richtext>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `active`  | Boolean | true   | Controls if the field is active and allowing input or disabled and in read-only mode |

## Properties
| Property | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `active`  | Boolean | true   | Controls if the field is active and allowing input or disabled and in read-only mode |
| `quill`  | Quill | Quill   | Get Only. A passthrough for  the Quill instance. can be used for extending functionality |
| `editior`  | Quill | Quill   | Get Only. A passthrough for  the Quill instance. can be used for extending functionality |
| `length`  | Number | 0 | Get Only. Returns the current length of the users input |
| `hasFocus`  | Boolean | false | Get Only. Returns True/False if the field is currently in the users focus |
| `styleTag`  | HTMLElement | HTMLElement | Get Only. Returns the componentized styles used for formatting the text |
| `styles`  | String | String | Get Only. Returns the styles as a string that are being used for formatting the text |
| `value`  | String | String | Get Only. Returns the pure HTML representation of the input |
| `contents`  | Delta | Delta | Can be used for setting or getting the contents of the Quill instance as a Delta |
| `text`  | String | String | Can be used for setting or getting the text contents of the Quill instance |

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `getLength`   | None | Gets the current length of the users input |
| `hasFocus`   | None | Returns True/False if the field is currently in the users focus |
| `focus`   | None | Sets the focus of the user to the editor |
| `blur`   | None | Removes focus from the editor |
| `getHTML`   | None | Returns the pure html associated with the input |
| `getContents`   | None | Returns the contents of the field in a Quill Delta |
| `setContents`   | Delta | Sets the contents of the field with a given Quill Delta |
| `getText`   | None | Returns the text of the field without formatting |
| `setText`   | String | Sets the text content of the field. For index insert use the editor passthrough |
| `dangerouslySetHTMLValue`   | String | Sets the HTML content of the field. Explicitly stated as dangerous, value property not available so use this if you need it |

## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| button           | The html button used inside the component |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```