import '../../styles/od-richtext-styles'
import Quill from '../../vendor/shadow-quill/quill.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>
        :host {
            display: block;

            height: 500px;
            min-width: 1050px;

            margin-bottom: 3em;
        }
    </style>

    <div id='container'>
        <od-richtext-styles id='styles'></od-richtext-styles>
    </div>

    <div id='editor-container' part='richtext-editor'></div>    
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-richtext" );

export class OdRichtext extends HTMLElement {
    
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        //set attribute default values first
        if ( !this.hasAttribute( "active" ) ) {
            this.setAttribute( "active", true );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( "active" );
        this._upgradeProperty( "value" );
        this._upgradeProperty( "contents" );
        this._upgradeProperty( "text" );

        //listeners and others etc.
        this._toolbar = this.shadowRoot.querySelector('#toolbar');
        var toolbarOptions = [
            [{ 'font': [] }, { 'size': [] },{ 'header': [] }],
            ['bold', 'italic', 'underline', 'strike'],
            [{ 'color': [] }, { 'background': [] }],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          
            [{ 'direction': 'rtl' }, { 'align': [] }],
            ['code', 'code-block', 'blockquote'],
            [{ 'script': 'sub'}, { 'script': 'super' }],
            ['link', 'image'],
          
            ['clean']                                         // remove formatting button
          ];
        let options = {
            modules: {
                toolbar: toolbarOptions
            },
            theme: 'snow'
        };
        let __scope = this;
        this._editor = new Quill( __scope, "#editor-container", options );

        this._addListeners();

        this.shadowRoot.querySelector( '#container' ).addEventListener( 'richtext-styles-ready', ( e ) => { this._applyStylesEvent( e ); } );
        let scopedStyles = this.shadowRoot.querySelector( "#styles" );
        this._setStyles( scopedStyles.styleTag, scopedStyles.styles );
    }

    _applyStylesEvent( e ) {
        this._setStyles( e.detail.styleTag, e.detail.styles );
    }

    _setStyles( styleTag, styles ) {
        if( this.styleTag !== undefined || !styleTag || !styles ) {
            return;
        }      
        this._styleTag = styleTag;
        this._styles = styles;
        this.shadowRoot.appendChild( this.styleTag );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["active"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "active": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setActiveAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setActiveAttribute( newValue );
                    this.setActive( newValue );
                }
                break;
            }
        }
    }

    get quill() {
        return this.editor;
    }

    get editor() {
        return this._editor;
    }

    get length() {        
        return this.getLength();
    }

    get hasFocus() {
        return this.hasFocus();
    }

    get styleTag() {
        return this._styleTag;
    }

    get styles() {
        return this._styles;
    }

    get value() {
        return this.getHTML()
    }

    set value( value ) {
        //dangerouslySet -- try not to implement this
    }

    get contents() {
        return this.getContents();
    }

    set contents( contents ) {
        this.setContents( contents );
    }

    get text() {        
        return this.getText();
    }

    set text( text ) {
        this.setText( text );
    }
    
    get active() {
        return this.hasAttribute( "active" );
    }

    set active( isActive ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setActiveAttribute( isActive );
    }

    _setActiveAttribute( newV ) {
        this._setBooleanAttribute( 'active', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    //Private

    _addListeners() {
        this.quill.on( 'text-change', ( delta, oldDelta, source ) => {
            this._dispatchChangeEvent( delta, oldDelta, source ) }
        );
    }

    _dispatchChangeEvent( delta, oldDelta, source ) {
        this.dispatchEvent(
            new CustomEvent( 'richtext-change', { 
                bubbles: true, 
                detail: { 
                    'delta': delta,
                    'oldDelta': oldDelta,
                    'source': source,
                    'value': this.value,
                    'styleTag': this.styleTag 
                }
            } )
        );
    }

    //Public

    getLength() {
        return this.quill.getLength();
    }

    hasFocus() {
        return this.quill.hasFocus();
    }

    getHTML() {
        return this.quill.root.innerHTML;
    }

    getContents() {
        let contents = this.quill.getContents();
        return contents;
    }

    setContents( contents ) {
        this.quill.setContents( contents, 'api' );
    }

    getText() {
        let text = this.quill.getText();
        return text;
    }

    setText( text ) {
        this.quill.setText( text );
    }

    blur() {
        this.quill.blur();
    }

    focus() {
        this.quill.focus();
    }

    setActive( isActive ) {        
        if( this.quill ) {
            this.quill.enable( isActive === true || isActive === 'true' );
        }
    }

    dangerouslySetHTMLValue( html ) {
        let delta = this.quill.clipboard.convert( html );
        this.setContents( delta, 'api' );
    }
}
